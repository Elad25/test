<?php

namespace app\models;

use Yii;
use app\models\Lead;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadId
 * @property string $name
 * @property integer $amount
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'leadId', 'name', 'amount'], 'required'],
            [['id', 'leadId', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadId' => 'Lead ID',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
	
	public function getDealItem() /////////////////////////
    {
        return $this->hasOne(Lead::className(), ['id' => 'leadId']);
    }
	
	
	public static function getDeals()  ////////////
	{
		$allDeals = self::find()->all();
		$allDealsArray = ArrayHelper::
					map($allDeals, 'id', 'name');
		return $allDealsArray;
	}
	
	//////////////////
	public function getLeads()
    {
        return $this->hasOne(Lead::className(), ['id' => 'leadId']);
    }
	//////////////////
}
