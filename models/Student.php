<?php

namespace app\models;
use \yii\base\Object;

class Student extends Object
{
    public $id;
    public $name;

    private static $students = [
        '1' => [
            'name' => 'Jack',
        ],
        '2' => [
            'name' => 'John',
        ],
    ];

	public function getName($id){
		return self::$students[$id]['name'];
	}

}
